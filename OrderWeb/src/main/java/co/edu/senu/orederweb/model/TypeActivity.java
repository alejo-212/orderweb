/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.senu.orederweb.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Aprendiz
 */
@Entity
@Table(name = "type_activity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeActivity.findAll", query = "SELECT t FROM TypeActivity t"),
    @NamedQuery(name = "TypeActivity.findByIdType", query = "SELECT t FROM TypeActivity t WHERE t.idType = :idType"),
    @NamedQuery(name = "TypeActivity.findByDescription", query = "SELECT t FROM TypeActivity t WHERE t.description = :description")})
public class TypeActivity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_type")
    private Integer idType;
    @Basic(optional = false)
    @Column(name = "description", nullable = false, length = 50)
    private String description;
    @OneToMany(mappedBy = "idType")
    private Collection<Activity> activityCollection;

    public TypeActivity() {
    }

    public TypeActivity(Integer idType) {
        this.idType = idType;
    }

    public TypeActivity(Integer idType, String description) {
        this.idType = idType;
        this.description = description;
    }

    public Integer getIdType() {
        return idType;
    }

    public void setIdType(Integer idType) {
        this.idType = idType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Activity> getActivityCollection() {
        return activityCollection;
    }

    public void setActivityCollection(Collection<Activity> activityCollection) {
        this.activityCollection = activityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idType != null ? idType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeActivity)) {
            return false;
        }
        TypeActivity other = (TypeActivity) object;
        if ((this.idType == null && other.idType != null) || (this.idType != null && !this.idType.equals(other.idType))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.senu.orederweb.model.TypeActivity[ idType=" + idType + " ]";
    }
    
}
