/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.senu.orederweb.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Aprendiz
 */
@Entity
@Table(name = "activity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Activity.findAll", query = "SELECT a FROM Activity a"),
    @NamedQuery(name = "Activity.findByIdActivity", query = "SELECT a FROM Activity a WHERE a.idActivity = :idActivity"),
    @NamedQuery(name = "Activity.findByDescription", query = "SELECT a FROM Activity a WHERE a.description = :description"),
    @NamedQuery(name = "Activity.findByHours", query = "SELECT a FROM Activity a WHERE a.hours = :hours"),
    @NamedQuery(name = "Activity.findByDate", query = "SELECT a FROM Activity a WHERE a.date = :date")})
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    
    @Column(name = "id_activity", nullable = false)
    private Integer idActivity;
    @Basic(optional = false)
    
 
    @Column(name = "description",nullable=false,length=100)
    private String description;
    @Basic(optional = false)
    @Column(name = "hours",nullable = false)
    private int hours;
    @Basic(optional = false)
    
    @Column(name = "date",nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;
    @JoinColumn(name = "id_technician", referencedColumnName = "document")
    @ManyToOne
    private Technician idTechnician;
    @JoinColumn(name = "id_type", referencedColumnName = "id_type")
    @ManyToOne
    private TypeActivity idType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "activity")
    private Collection<OrderActivity> orderActivityCollection;

    public Activity() {
    }

    public Activity(Integer idActivity) {
        this.idActivity = idActivity;
    }

    public Activity(Integer idActivity, String description, int hours, Date date) {
        this.idActivity = idActivity;
        this.description = description;
        this.hours = hours;
        this.date = date;
    }

    public Integer getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(Integer idActivity) {
        this.idActivity = idActivity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Technician getIdTechnician() {
        return idTechnician;
    }

    public void setIdTechnician(Technician idTechnician) {
        this.idTechnician = idTechnician;
    }

    public TypeActivity getIdType() {
        return idType;
    }

    public void setIdType(TypeActivity idType) {
        this.idType = idType;
    }

    @XmlTransient
    public Collection<OrderActivity> getOrderActivityCollection() {
        return orderActivityCollection;
    }

    public void setOrderActivityCollection(Collection<OrderActivity> orderActivityCollection) {
        this.orderActivityCollection = orderActivityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idActivity != null ? idActivity.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Activity)) {
            return false;
        }
        Activity other = (Activity) object;
        if ((this.idActivity == null && other.idActivity != null) || (this.idActivity != null && !this.idActivity.equals(other.idActivity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.senu.orederweb.model.Activity[ idActivity=" + idActivity + " ]";
    }
    
}
