/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.senu.orederweb.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aprendiz
 */
@Entity
@Table(name = "order_activity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrderActivity.findAll", query = "SELECT o FROM OrderActivity o"),
    @NamedQuery(name = "OrderActivity.findByIdOrder", query = "SELECT o FROM OrderActivity o WHERE o.orderActivityPK.idOrder = :idOrder"),
    @NamedQuery(name = "OrderActivity.findByIdActivity", query = "SELECT o FROM OrderActivity o WHERE o.orderActivityPK.idActivity = :idActivity"),
    @NamedQuery(name = "OrderActivity.findBySerial", query = "SELECT o FROM OrderActivity o WHERE o.serial = :serial")})
public class OrderActivity implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrderActivityPK orderActivityPK;
    @Basic(optional = false)
    @Column(name = "serial", nullable = false)
    private int serial;
    @JoinColumn(name = "id_order", referencedColumnName = "id_order", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Order1 order1;
    @JoinColumn(name = "id_activity", referencedColumnName = "id_activity", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Activity activity;

    public OrderActivity() {
    }

    public OrderActivity(OrderActivityPK orderActivityPK) {
        this.orderActivityPK = orderActivityPK;
    }

    public OrderActivity(OrderActivityPK orderActivityPK, int serial) {
        this.orderActivityPK = orderActivityPK;
        this.serial = serial;
    }

    public OrderActivity(int idOrder, int idActivity) {
        this.orderActivityPK = new OrderActivityPK(idOrder, idActivity);
    }

    public OrderActivityPK getOrderActivityPK() {
        return orderActivityPK;
    }

    public void setOrderActivityPK(OrderActivityPK orderActivityPK) {
        this.orderActivityPK = orderActivityPK;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public Order1 getOrder1() {
        return order1;
    }

    public void setOrder1(Order1 order1) {
        this.order1 = order1;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderActivityPK != null ? orderActivityPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderActivity)) {
            return false;
        }
        OrderActivity other = (OrderActivity) object;
        if ((this.orderActivityPK == null && other.orderActivityPK != null) || (this.orderActivityPK != null && !this.orderActivityPK.equals(other.orderActivityPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.senu.orederweb.model.OrderActivity[ orderActivityPK=" + orderActivityPK + " ]";
    }
    
}
