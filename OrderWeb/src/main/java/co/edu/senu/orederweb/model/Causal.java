/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.senu.orederweb.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Aprendiz
 */
@Entity
@Table(name = "causal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Causal.findAll", query = "SELECT c FROM Causal c"),
    @NamedQuery(name = "Causal.findByIdCausal", query = "SELECT c FROM Causal c WHERE c.idCausal = :idCausal"),
    @NamedQuery(name = "Causal.findByDescription", query = "SELECT c FROM Causal c WHERE c.description = :description")})
public class Causal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_causal")
    private Integer idCausal;
    @Basic(optional = false)
    @Column(name = "description",nullable = false, length = 50)
    private String description;
    @OneToMany(mappedBy = "idCausal")
    private Collection<Order1> order1Collection;

    public Causal() {
    }

    public Causal(Integer idCausal) {
        this.idCausal = idCausal;
    }

    public Causal(Integer idCausal, String description) {
        this.idCausal = idCausal;
        this.description = description;
    }

    public Integer getIdCausal() {
        return idCausal;
    }

    public void setIdCausal(Integer idCausal) {
        this.idCausal = idCausal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Order1> getOrder1Collection() {
        return order1Collection;
    }

    public void setOrder1Collection(Collection<Order1> order1Collection) {
        this.order1Collection = order1Collection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCausal != null ? idCausal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Causal)) {
            return false;
        }
        Causal other = (Causal) object;
        if ((this.idCausal == null && other.idCausal != null) || (this.idCausal != null && !this.idCausal.equals(other.idCausal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.senu.orederweb.model.Causal[ idCausal=" + idCausal + " ]";
    }
    
}
