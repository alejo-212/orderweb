/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.senu.orederweb.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Aprendiz
 */
@Entity
@Table(name = "observation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Observation.findAll", query = "SELECT o FROM Observation o"),
    @NamedQuery(name = "Observation.findByIdObservation", query = "SELECT o FROM Observation o WHERE o.idObservation = :idObservation"),
    @NamedQuery(name = "Observation.findByDescription", query = "SELECT o FROM Observation o WHERE o.description = :description")})
public class Observation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_observation")
    private Integer idObservation;
    @Basic(optional = false)
    @Column(name = "description",nullable = false, length = 50)
    private String description;
    @OneToMany(mappedBy = "idObservation")
    private Collection<Order1> order1Collection;

    public Observation() {
    }

    public Observation(Integer idObservation) {
        this.idObservation = idObservation;
    }

    public Observation(Integer idObservation, String description) {
        this.idObservation = idObservation;
        this.description = description;
    }

    public Integer getIdObservation() {
        return idObservation;
    }

    public void setIdObservation(Integer idObservation) {
        this.idObservation = idObservation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Order1> getOrder1Collection() {
        return order1Collection;
    }

    public void setOrder1Collection(Collection<Order1> order1Collection) {
        this.order1Collection = order1Collection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idObservation != null ? idObservation.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Observation)) {
            return false;
        }
        Observation other = (Observation) object;
        if ((this.idObservation == null && other.idObservation != null) || (this.idObservation != null && !this.idObservation.equals(other.idObservation))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.senu.orederweb.model.Observation[ idObservation=" + idObservation + " ]";
    }
    
}
