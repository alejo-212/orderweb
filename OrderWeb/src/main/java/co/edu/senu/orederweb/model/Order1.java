/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.senu.orederweb.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Aprendiz
 */
@Entity
@Table(name = "order1")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Order1.findAll", query = "SELECT o FROM Order1 o"),
    @NamedQuery(name = "Order1.findByIdOrder", query = "SELECT o FROM Order1 o WHERE o.idOrder = :idOrder"),
    @NamedQuery(name = "Order1.findByLegalizationDate", query = "SELECT o FROM Order1 o WHERE o.legalizationDate = :legalizationDate"),
    @NamedQuery(name = "Order1.findByAddress", query = "SELECT o FROM Order1 o WHERE o.address = :address"),
    @NamedQuery(name = "Order1.findByCity", query = "SELECT o FROM Order1 o WHERE o.city = :city"),
    @NamedQuery(name = "Order1.findByState", query = "SELECT o FROM Order1 o WHERE o.state = :state")})
public class Order1 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_order", nullable = false)
    private Integer idOrder;
    @Basic(optional = false)
    @Column(name = "legalization_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date legalizationDate;
    @Basic(optional = false)
    @Column(name = "address",nullable = false, length = 50)
    private String address;
    @Basic(optional = false)
    @Column(name = "city", nullable = false, length = 50)
    private String city;
    @Basic(optional = false)
    @Column(name = "state", nullable = false, length = 50)
    private String state;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "order1")
    private Collection<OrderActivity> orderActivityCollection;
    @JoinColumn(name = "id_causal", referencedColumnName = "id_causal")
    @ManyToOne
    private Causal idCausal;
    @JoinColumn(name = "id_observation", referencedColumnName = "id_observation")
    @ManyToOne
    private Observation idObservation;

    public Order1() {
    }

    public Order1(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public Order1(Integer idOrder, Date legalizationDate, String address, String city, String state) {
        this.idOrder = idOrder;
        this.legalizationDate = legalizationDate;
        this.address = address;
        this.city = city;
        this.state = state;
    }

    public Integer getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public Date getLegalizationDate() {
        return legalizationDate;
    }

    public void setLegalizationDate(Date legalizationDate) {
        this.legalizationDate = legalizationDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @XmlTransient
    public Collection<OrderActivity> getOrderActivityCollection() {
        return orderActivityCollection;
    }

    public void setOrderActivityCollection(Collection<OrderActivity> orderActivityCollection) {
        this.orderActivityCollection = orderActivityCollection;
    }

    public Causal getIdCausal() {
        return idCausal;
    }

    public void setIdCausal(Causal idCausal) {
        this.idCausal = idCausal;
    }

    public Observation getIdObservation() {
        return idObservation;
    }

    public void setIdObservation(Observation idObservation) {
        this.idObservation = idObservation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOrder != null ? idOrder.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Order1)) {
            return false;
        }
        Order1 other = (Order1) object;
        if ((this.idOrder == null && other.idOrder != null) || (this.idOrder != null && !this.idOrder.equals(other.idOrder))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.edu.senu.orederweb.model.Order1[ idOrder=" + idOrder + " ]";
    }
    
}
