/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.orderweb.persistence;

import co.edu.senu.orederweb.model.Causal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class CausalDAO implements ICausalDAO {
    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public void insert(Causal causal) throws Exception {
        try {
            entityManager.persist(causal);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Causal causal) throws Exception {
    try {
            entityManager.merge(causal);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void delete(Causal causal) throws Exception {
   try {
            entityManager.remove(causal);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public Causal findById(Integer id_causal) throws Exception {
    try {
            return entityManager.find(Causal.class, id_causal);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<Causal> findAll() throws Exception {
    try {
        Query query = entityManager.createNamedQuery("Causal.findAll");
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
