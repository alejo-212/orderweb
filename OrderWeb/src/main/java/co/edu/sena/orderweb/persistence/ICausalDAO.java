/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package co.edu.sena.orderweb.persistence;

import co.edu.senu.orederweb.model.Causal;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aprendiz
 */
@Local
public interface ICausalDAO {
    public void insert (Causal causal) throws Exception;
    public void update (Causal causal) throws  Exception;
    public void delete (Causal causal) throws Exception;
    public Causal findById(Integer id_causal) throws Exception;
    public List<Causal> findAll() throws Exception;
}
