-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.44 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para bdorder
CREATE DATABASE IF NOT EXISTS `bdorder` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `bdorder`;

-- Volcando estructura para tabla bdorder.activity
CREATE TABLE IF NOT EXISTS `activity` (
  `id_activity` int(11) NOT NULL COMMENT 'id actividad',
  `description` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripción de la actividad',
  `hours` int(11) NOT NULL COMMENT 'horas estimadas de duración',
  `date` date NOT NULL COMMENT 'fecha',
  `id_technician` bigint(20) DEFAULT NULL COMMENT 'id técnico que hace la actividad',
  `id_type` int(11) DEFAULT NULL COMMENT 'id tipo de actividad',
  PRIMARY KEY (`id_activity`),
  KEY `id_technician` (`id_technician`),
  KEY `id_type` (`id_type`),
  CONSTRAINT `activity_ibfk_1` FOREIGN KEY (`id_technician`) REFERENCES `technician` (`document`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `activity_ibfk_2` FOREIGN KEY (`id_type`) REFERENCES `type_activity` (`id_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de actividades';

-- Volcando datos para la tabla bdorder.activity: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
INSERT INTO `activity` (`id_activity`, `description`, `hours`, `date`, `id_technician`, `id_type`) VALUES
	(100, 'Cambiar el contador', 8, '2020-01-23', 1001, 5),
	(200, 'Suspender el servicio por no pago', 2, '2019-10-02', 1004, 4),
	(300, 'Reconectar el servicio', 2, '2020-02-05', 1000, 4),
	(400, 'Construir e instalar el medidor', 10, '2019-11-13', 1003, 2),
	(500, 'Lectura del medidor', 1, '2019-06-20', 1002, 5);
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;

-- Volcando estructura para tabla bdorder.causal
CREATE TABLE IF NOT EXISTS `causal` (
  `id_causal` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id causal',
  `description` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripción de la causal',
  PRIMARY KEY (`id_causal`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla causales de la actividad';

-- Volcando datos para la tabla bdorder.causal: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `causal` DISABLE KEYS */;
INSERT INTO `causal` (`id_causal`, `description`) VALUES
	(1, 'Reparación contador'),
	(2, 'Suspensión del servicio'),
	(3, 'Reconexión del servicio'),
	(4, 'Instalación del contador'),
	(5, 'Cambio del contador');
/*!40000 ALTER TABLE `causal` ENABLE KEYS */;

-- Volcando estructura para tabla bdorder.observation
CREATE TABLE IF NOT EXISTS `observation` (
  `id_observation` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id observacion',
  `description` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripción de la observación',
  PRIMARY KEY (`id_observation`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla observaciones no cumplimiento';

-- Volcando datos para la tabla bdorder.observation: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `observation` DISABLE KEYS */;
INSERT INTO `observation` (`id_observation`, `description`) VALUES
	(1, 'Perro bravo'),
	(2, 'Contador con candado'),
	(3, 'Contador inaccesible'),
	(4, 'Predio en construcción'),
	(5, 'No existe contador');
/*!40000 ALTER TABLE `observation` ENABLE KEYS */;

-- Volcando estructura para tabla bdorder.order1
CREATE TABLE IF NOT EXISTS `order1` (
  `id_order` int(11) NOT NULL COMMENT 'id orden',
  `legalization_date` date NOT NULL COMMENT 'fecha legalización',
  `address` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'dirección',
  `city` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'ciudad',
  `state` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'departamento o estado',
  `id_observation` int(11) DEFAULT NULL COMMENT 'id observacion',
  `id_causal` int(11) DEFAULT NULL COMMENT 'id causal',
  PRIMARY KEY (`id_order`),
  KEY `id_causal` (`id_causal`),
  KEY `id_observation` (`id_observation`),
  CONSTRAINT `order1_ibfk_1` FOREIGN KEY (`id_causal`) REFERENCES `causal` (`id_causal`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order1_ibfk_2` FOREIGN KEY (`id_observation`) REFERENCES `observation` (`id_observation`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de orden de trabajo';

-- Volcando datos para la tabla bdorder.order1: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `order1` DISABLE KEYS */;
INSERT INTO `order1` (`id_order`, `legalization_date`, `address`, `city`, `state`, `id_observation`, `id_causal`) VALUES
	(1, '2020-02-23', 'Av. Siempre Viva', 'Springfield', 'Massachusets', NULL, 1),
	(2, '2019-10-02', 'Calle falsa 123', 'Springfield', 'Massachusets', 1, 2),
	(3, '2020-02-06', 'Cra 10 # 3-4', 'Tuluá', 'Valle', NULL, 3),
	(4, '2019-11-14', 'Cll 20 # 21-22', 'Cali', 'Valle', NULL, 4),
	(5, '2019-06-21', 'Av. Caracas # 7-89', 'Bogotá', 'Bogotá D.C.', NULL, 5);
/*!40000 ALTER TABLE `order1` ENABLE KEYS */;

-- Volcando estructura para tabla bdorder.technician
CREATE TABLE IF NOT EXISTS `technician` (
  `document` bigint(20) NOT NULL COMMENT 'cedula del técnico',
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'nombre completo',
  `especiality` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'especialidad del técnico',
  `phone` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'teléfono',
  PRIMARY KEY (`document`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de técnicos';

-- Volcando datos para la tabla bdorder.technician: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `technician` DISABLE KEYS */;
INSERT INTO `technician` (`document`, `name`, `especiality`, `phone`) VALUES
	(1000, 'Lola Mento', NULL, '232'),
	(1001, 'Helen Chufe', 'Instalación de redes', '225'),
	(1002, 'Matías Queroso', NULL, '231'),
	(1003, 'Paul Vazo', 'Construcción', '230'),
	(1004, 'Rubén Fermizo', 'Lectura de redes', '233');
/*!40000 ALTER TABLE `technician` ENABLE KEYS */;

-- Volcando estructura para tabla bdorder.type_activity
CREATE TABLE IF NOT EXISTS `type_activity` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id tipo de actividad',
  `description` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripción',
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de tipos de actividad';

-- Volcando datos para la tabla bdorder.type_activity: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `type_activity` DISABLE KEYS */;
INSERT INTO `type_activity` (`id_type`, `description`) VALUES
	(1, 'REPARACION'),
	(2, 'CONSTRUCCION'),
	(3, 'INSTALACION'),
	(4, 'SUSPENSION'),
	(5, 'OTROS');
/*!40000 ALTER TABLE `type_activity` ENABLE KEYS */;

-- Volcando estructura para tabla bdorder.users
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'nombre de usuario',
  `password` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'contraseña del usuario',
  `role` varchar(50) COLLATE utf8_spanish_ci NOT NULL COMMENT 'rol o tipo de usuario. SUPERVISOR, ADMINISTRADOR',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de usuarios';

-- Volcando datos para la tabla bdorder.users: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`username`, `password`, `role`) VALUES
	('Homero J. Simpson', '456', 'SUPERVISOR'),
	('Montgomery Burns', '123', 'ADMINISTRADOR');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
